<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index(){
        $articles = Article::all();
        return view('article.index' , compact('articles'));
    }

    public function addArticle(){
        return view('article.addArticle');
    }

    public function storeArticle(Request $request){
        $article = new Article();
        $article->name = isset( $request->name ) ? $request->name : "";
        $article->updated_at = Null;
        $article->save();

        return redirect()->route('article')->with('success' , 'Article added successfully.');
    }

    public function deleteArticle($id){
        Article::where('id',$id)->delete();

        return redirect()->route('article')->with('error' , 'Article deleted!');
    }

    public function editArticle($id){
        $article = Article::where('id',$id)->first();
        return view('article.editArticle' , compact('article'));
    }

    public function updateArticle($id,Request $request){
        $article = Article::findOrFail($id);
        $article->name = isset( $request->name ) ? $request->name : "";
        $article->created_at = isset( $article->created_at ) ? $article->created_at : "";
        $article->updated_at = now();
        $article->save();

        return redirect()->route('article')->with('success' , 'Article updated successfully.');
    }
}
