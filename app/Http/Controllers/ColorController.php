<?php

namespace App\Http\Controllers;

use App\Models\Color;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    public function index(){
        $colors = Color::all();
        return view('color.index' , compact('colors'));
    }

    public function addColor(){
        return view('color.addColor');
    }

    public function storeColor(Request $request){
        $color = new Color();
        $color->name = isset( $request->name ) ? $request->name : "";
        $color->updated_at = Null;
        $color->save();

        return redirect()->route('color')->with('success' , 'Color added successfully.');
    }

    public function deleteColor($id){
        Color::where('id',$id)->delete();

        return redirect()->route('color')->with('error' , 'Color deleted!');
    }

    public function editColor($id){
        $color = Color::where('id',$id)->first();
        return view('color.editColor' , compact('color'));
    }

    public function updateColor($id,Request $request){
        $color = Color::findOrFail($id);
        $color->name = isset( $request->name ) ? $request->name : "";
        $color->created_at = isset( $color->created_at ) ? $color->created_at : "";
        $color->updated_at = now();
        $color->save();

        return redirect()->route('color')->with('success' , 'Color updated successfully.');
    }
}
