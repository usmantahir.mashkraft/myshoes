<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function index(){
        $companies = Company::all();
        return view('company.index' , compact('companies'));
    }

    public function addCompany(){
        return view('company.addCompany');
    }

    public function storeCompany(Request $request){
        $company = new Company;
        $company->name = isset( $request->company_name ) ? $request->company_name : "";
        $company->updated_at = Null;
        $company->save();

        return redirect()->route('company')->with('success' , 'Company added successfully.');
    }

    public function deleteCompany($id){
        Company::where('id',$id)->delete();

        return redirect()->route('company')->with('error' , 'Company deleted!');
    }

    public function editCompany($id){
        $company = Company::where('id',$id)->first();
        return view('company.editCompany' , compact('company'));
    }

    public function updateCompany($id,Request $request){
        $company = Company::findOrFail($id);
        $company->name = isset( $request->company_name ) ? $request->company_name : "";
        $company->created_at = isset( $company->created_at ) ? $company->created_at : "";
        $company->updated_at = now();
        $company->save();

        return redirect()->route('company')->with('success' , 'Company updated successfully.');
    }
}
