<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Color;
use App\Models\Company;
use App\Models\Product;
use App\Models\Size;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index(){
         $products = Product::with('company:id,name')
            ->with('size:id,name')
            ->with('color:id,name')
            ->with('article:id,name')
             ->latest()
            ->get();

        return view('product.index' , compact('products'));
    }

    public function addProduct(){
        $companies = Company::all(['id','name']);
        $sizes = Size::all(['id','name']);
        $articles = Article::all(['id','name']);
        $colors = Color::all(['id','name']);

        return view('product.addProduct' , compact('companies','sizes', 'articles', 'colors'));
    }

    public function storeProduct(Request $request){
        DB::beginTransaction();
        try {
            $product = new Product;
            $product->company_id = isset( $request->company_id ) ? (int) $request->company_id : 0;
            $product->size_id = isset( $request->size_id ) ? (int) $request->size_id : 0;
            $product->color_id = isset( $request->color_id ) ? (int) $request->color_id : 0;
            $product->article_id = isset( $request->article_id ) ? (int) $request->article_id : 0;
            $product->sale_price = isset( $request->sale_price ) ? (int) $request->sale_price : 0;
            $product->original_price = isset( $request->original_price ) ? (int) $request->original_price : 0;
            $product->quantity = isset( $request->quantity ) ? (int) $request->quantity : 0;

            $product->save();

            DB::commit();

            return redirect()->route('product')->with('success' , 'Product added successfully!');
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function deleteProduct($id){
        Product::where('id',$id)->delete();

        return redirect()->route('product')->with('error' , 'Product deleted!');
    }

    public function editProduct($id){
        $product = Product::where('id',$id)
            ->first();

        $companies = Company::all(['id','name']);
        $sizes = Size::all(['id','name']);
        $articles = Article::all(['id','name']);
        $colors = Color::all(['id','name']);

        return view('product.editProduct' , compact('product','companies','sizes', 'articles', 'colors'));
    }

    public function updateProduct(Request $request,$id){
        DB::beginTransaction();
        try {
            $product = Product::findOrFail($id);
            $product->company_id = isset( $request->company_id ) ? (int) $request->company_id : 0;
            $product->size_id = isset( $request->size_id ) ? (int) $request->size_id : 0;
            $product->color_id = isset( $request->color_id ) ? (int) $request->color_id : 0;
            $product->article_id = isset( $request->article_id ) ? (int) $request->article_id : 0;
            $product->sale_price = isset( $request->sale_price ) ? (int) $request->sale_price : 0;
            $product->original_price = isset( $request->original_price ) ? (int) $request->original_price : 0;
            $product->quantity = isset( $request->quantity ) ? (int) $request->quantity : 0;

            $product->save();

            DB::commit();

            return redirect()->route('product')->with('success' , 'Product updated successfully!');
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }
}
