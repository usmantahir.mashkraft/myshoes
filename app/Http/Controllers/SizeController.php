<?php

namespace App\Http\Controllers;

use App\Models\Size;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    public function index(){
        $sizes = Size::all();
        return view('size.index' , compact('sizes'));
    }

    public function addSize(){
        return view('size.addSize');
    }

    public function storeSize(Request $request){
        $size = new Size;
        $size->name = isset( $request->name ) ? $request->name : "";
        $size->updated_at = Null;
        $size->save();

        return redirect()->route('size')->with('success' , 'Size added successfully.');
    }

    public function deleteSize($id){
        Size::where('id',$id)->delete();

        return redirect()->route('size')->with('error' , 'Size deleted!');
    }

    public function editSize($id){
        $size = Size::where('id',$id)->first();
        return view('size.editSize' , compact('size'));
    }

    public function updateSize($id,Request $request){
        $size = Size::findOrFail($id);
        $size->name = isset( $request->name ) ? $request->name : "";
        $size->created_at = isset( $size->created_at ) ? $size->created_at : "";
        $size->updated_at = now();
        $size->save();

        return redirect()->route('size')->with('success' , 'Size updated successfully.');
    }
}
