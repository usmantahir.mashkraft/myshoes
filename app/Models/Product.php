<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function company(){
        return $this->belongsTo(Company::class,'company_id');
    }

    public function size(){
        return $this->belongsTo(Size::class,'size_id');
    }

    public function color(){
        return $this->belongsTo(Color::class,'color_id');
    }

    public function article(){
        return $this->belongsTo(Article::class,'article_id');
    }
}
