@extends('layout.app')
@section('content')
    @push('css')
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        <style>
            .select2-container {
                width: 100% !important;
            }

            .select2-container--default .select2-selection--single .select2-selection__rendered {
                color: #444;
                line-height: 42px !important;
            }

            .select2-container--default .select2-selection--single .select2-selection__clear {
                margin-top: 8px !important;
            }

            .select2-container {
                height: 3.1rem !important;
                border: 1px solid #ced4da !important;
                border-color: #ebedf2 !important;
                border-radius: 0.25rem !important;
                border-width: 2px !important;
            }

            .select2-selection--single {
                line-height: 1.25 !important;
            }

            .select2-container .select2-selection--single {
                height: 3.1rem !important;
                border: 1px solid #ced4da !important;
                border-color: #ebedf2 !important;
                border-radius: 0 !important;
                border-width: 2px !important;
            }
        </style>
    @endpush
    <!--**********************************
            Content body start
        ***********************************-->
    <div class="content-body">
        <div class="page-titles">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Edit Product</a></li>
            </ol>
        </div>
        <div class="container-fluid">

            <!-- row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Edit Product</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-validation">
                                <form method="post" action="{{ route('update.product' , $product->id) }}" id="myForm" class="needs-validation" autocomplete="off" >
                                    @csrf
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <div class="mb-3 row">
                                                <label class="col-lg-2 col-form-label" for="validationCustom01">Select Company
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <div class="col-lg-6">
                                                    <select name="company_id" id="company_id" class="form-control">
                                                        <option value="0" selected disabled>Select a company</option>
                                                        @foreach($companies as $company)
                                                            <option @if(isset( $product->company_id ) && $product->company_id == $company->id ) selected @endif value="{{ $company->id }}">{{ isset( $company->name ) ? $company->name : "" }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="mb-3 row">
                                                <label class="col-lg-2 col-form-label" for="validationCustom01">Select Size
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <div class="col-lg-6">
                                                    <select name="size_id" id="size_id" class="form-control">
                                                        <option selected disabled>Select a Size</option>
                                                        @foreach($sizes as $size)
                                                            <option @if(isset( $product->size_id ) && $product->size_id == $size->id ) selected @endif value="{{ $size->id }}">{{ isset( $size->name ) ? $size->name : "" }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="mb-3 row">
                                                <label class="col-lg-2 col-form-label" for="validationCustom01">Select Color
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <div class="col-lg-6">
                                                    <select name="color_id" id="color_id" class="form-control">
                                                        <option selected disabled>Select a Color</option>
                                                        @foreach($colors as $color)
                                                            <option @if(isset( $product->color_id ) && $product->color_id == $color->id ) selected @endif value="{{ $color->id }}">{{ isset( $color->name ) ? $color->name : "" }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="mb-3 row">
                                                <label class="col-lg-2 col-form-label" for="validationCustom01">Select Article
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <div class="col-lg-6">
                                                    <select name="article_id" id="article_id" class="form-control">
                                                        <option selected disabled>Select a Article</option>
                                                        @foreach($articles as $article)
                                                            <option @if(isset( $product->article_id ) && $product->article_id == $article->id ) selected @endif value="{{ $article->id }}">{{ isset( $article->name ) ? $article->name : "" }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="mb-3 row">
                                                <label class="col-lg-2 col-form-label" for="validationCustom01">Enter Original Price
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <div class="col-lg-6">
                                                    <input value="{{ isset( $product->original_price ) ? $product->original_price : 0 }}" id="original_price" type="number" class="form-control"  placeholder="Enter Original Price ..."  name="original_price">
                                                </div>
                                            </div>

                                            <div class="mb-3 row">
                                                <label class="col-lg-2 col-form-label" for="validationCustom01">Enter Sales Price
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <div class="col-lg-6">
                                                    <input value="{{ isset( $product->sale_price ) ? $product->sale_price : 0 }}" id="sale_price" type="number" class="form-control"  placeholder="Enter Sales Price ..."  name="sale_price">
                                                </div>
                                            </div>

                                            <div class="mb-3 row">
                                                <label class="col-lg-2 col-form-label" for="validationCustom01">Enter Quantity
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <div class="col-lg-6">
                                                    <input value="{{ isset( $product->quantity ) ? $product->quantity : 0 }}" id="quantity" type="number" class="form-control"  placeholder="Enter Quantity ..."  name="quantity">
                                                </div>
                                            </div>

                                            <div class="mb-3 row">
                                                <div class="col-lg-2 col-form-label"></div>
                                                <div class="col-lg-8">
                                                    <button type="submit" class="btn btn-primary">Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--**********************************
        Content body end
    ***********************************-->

    @push('js')
        <script>
            $(document).ready(function(){
                $('#myForm').on('submit', function(e){
                    e.preventDefault();

                    var error_found = false;

                    var company_id = $('#company_id');
                    var size_id = $('#size_id');
                    var color_id = $('#color_id');
                    var article_id = $('#article_id');
                    var original_price = $('#original_price');
                    var sale_price = $('#sale_price');
                    var quantity = $('#quantity');

                    if ( company_id.val() < 0 || company_id.val() == null ){
                        company_id.addClass('is-invalid');
                        error_found = false;
                    }else {
                        company_id.removeClass('is-invalid');
                        error_found = true;
                    }

                    if ( size_id.val() < 0 || size_id.val() == null ){
                        size_id.addClass('is-invalid');
                        error_found = false;
                    }else {
                        size_id.removeClass('is-invalid');
                        error_found = true;
                    }

                    if ( color_id.val() < 0 || color_id.val() == null ){
                        color_id.addClass('is-invalid');
                        error_found = false;
                    }else {
                        color_id.removeClass('is-invalid');
                        error_found = true;
                    }

                    if ( article_id.val() < 0 || article_id.val() == null ){
                        article_id.addClass('is-invalid');
                        error_found = false;
                    }else {
                        article_id.removeClass('is-invalid');
                        error_found = true;
                    }

                    if ( original_price.val() == 0 || original_price.val() == null ){
                        original_price.addClass('is-invalid');
                        error_found = false;
                    }else {
                        original_price.removeClass('is-invalid');
                        error_found = true;
                    }

                    if ( sale_price.val() == 0 || sale_price.val() == null ){
                        sale_price.addClass('is-invalid');
                        error_found = false;
                    }else {
                        sale_price.removeClass('is-invalid');
                        error_found = true;
                    }

                    if ( quantity.val() == 0 || quantity.val() == null ){
                        quantity.addClass('is-invalid');
                        error_found = false;
                    }else {
                        quantity.removeClass('is-invalid');
                        error_found = true;
                    }

                    if ( error_found === true ){
                        this.submit()
                    }else {
                        return false;
                    }
                });
            });
        </script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
        <script>
            $('#company_id, #size_id, #article_id', '#color_id').select2({
                allowClear: true,
            })
        </script>
    @endpush

@endsection
