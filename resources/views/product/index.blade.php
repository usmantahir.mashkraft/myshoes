@extends('layout.app')
@section('content')
    <!--**********************************
            Content body start
        ***********************************-->
    <div class="content-body">
        <div class="page-titles">
            <ol class="breadcrumb">
                <li class="breadcrumb-item "><a href="javascript:void(0)">Product Listing</a></li>
            </ol>
        </div>
        <!-- container starts -->
        <div class="container-fluid">

            <!-- row -->
{{--            <div class="element-area">--}}
            <div class="">
                @include('error.message')
                <div class="demo-view">
                    <div class="container-fluid pt-0 ps-0 pe-lg-4 pe-0">
                        <!-- Column starts -->
                        <div class="col-xl-12">
                            <div class="card dz-card" id="accordion-four">
                                <div class="card-header flex-wrap d-flex justify-content-between">
                                    <div>
                                        <h4 class="card-title">Product Listing</h4>
                                    </div>
                                </div>

                                <!-- /tab-content -->
                                <div class="tab-content" id="myTabContent-3">
                                    <div class="tab-pane fade show active" id="withoutBorder" role="tabpanel" aria-labelledby="home-tab-3">
                                        <div class="card-body pt-0">
                                            <div class="table-responsive">
                                                <table id="example4" class="display table" style="min-width: 845px">
                                                    <thead>
                                                    <tr>
                                                        <th>SR No. </th>
                                                        <th>Company </th>
                                                        <th>Size </th>
                                                        <th>Color </th>
                                                        <th>Article No. </th>
                                                        <th>Original Price </th>
                                                        <th>Sales Price </th>
                                                        <th>Quantity </th>
                                                        <th>Created At </th>
                                                        <th>Updated At </th>
                                                        <th>Action </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($products as $index => $row)
                                                        <tr>
                                                            <td>{{ ++$index }}</td>
                                                            <td>{{ isset( $row->company->name ) ? $row->company->name : "" }}</td>
                                                            <td>{{ isset( $row->size->name ) ? $row->size->name : "" }}</td>
                                                            <td>{{ isset( $row->color->name ) ? $row->color->name : "" }}</td>
                                                            <td>{{ isset( $row->article->name ) ? $row->article->name : "" }}</td>
                                                            <td>{{ isset( $row->original_price ) ? $row->original_price : 0 }}</td>
                                                            <td>{{ isset( $row->sale_price ) ? $row->sale_price : 0 }}</td>
                                                            <td>{{ isset( $row->quantity ) ? $row->quantity : 0 }}</td>
                                                            <td>{{ isset( $row->created_at ) ? $row->created_at : "" }}</td>
                                                            <td>{{ isset( $row->updated_at ) ? $row->updated_at : "NA" }}</td>
                                                            <td>
                                                                <div class="dropdown ms-auto text-end c-pointer text-center">
                                                                    <div class="btn-link" data-bs-toggle="dropdown">
                                                                        <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
                                                                    </div>
                                                                    <div class="dropdown-menu dropdown-menu-end">
                                                                        <a class="dropdown-item" href="{{ route('edit.product' , $row->id) }}">Edit</a>
                                                                        <a class="dropdown-item" href="{{ route('delete.product',$row->id) }}">Delete</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /tab-content -->

                            </div>
                        </div>
                        <!-- Column ends -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--**********************************
            Content body end
        ***********************************-->
@endsection
