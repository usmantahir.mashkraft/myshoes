<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\CompanyController;
use \App\Http\Controllers\DashboardController;
use \App\Http\Controllers\SizeController;
use \App\Http\Controllers\ArticleController;
use \App\Http\Controllers\ColorController;
use \App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('dashboard.dashboard');
//});

//Dashboard
Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

//Company
Route::get('company', [CompanyController::class, 'index'])->name('company');
Route::get('add-company', [CompanyController::class, 'addCompany'])->name('add.company');
Route::post('store-company', [CompanyController::class, 'storeCompany'])->name('store.company');
Route::get('delete-company/{id}', [CompanyController::class, 'deleteCompany'])->name('delete.company');
Route::get('edit-company/{id}', [CompanyController::class, 'editCompany'])->name('edit.company');
Route::post('update-company/{id}', [CompanyController::class, 'updateCompany'])->name('update.company');

//Size
Route::get('size', [SizeController::class, 'index'])->name('size');
Route::get('add-size', [SizeController::class, 'addSize'])->name('add.size');
Route::post('store-size', [SizeController::class, 'storeSize'])->name('store.size');
Route::get('delete-size/{id}', [SizeController::class, 'deleteSize'])->name('delete.size');
Route::get('edit-size/{id}', [SizeController::class, 'editSize'])->name('edit.size');
Route::post('update-size/{id}', [SizeController::class, 'updateSize'])->name('update.size');

//Article
Route::get('article', [ArticleController::class, 'index'])->name('article');
Route::get('add-article', [ArticleController::class, 'addArticle'])->name('add.article');
Route::post('store-article', [ArticleController::class, 'storeArticle'])->name('store.article');
Route::get('delete-article/{id}', [ArticleController::class, 'deleteArticle'])->name('delete.article');
Route::get('edit-article/{id}', [ArticleController::class, 'editArticle'])->name('edit.article');
Route::post('update-article/{id}', [ArticleController::class, 'updateArticle'])->name('update.article');

//Color
Route::get('color', [ColorController::class, 'index'])->name('color');
Route::get('add-color', [ColorController::class, 'addColor'])->name('add.color');
Route::post('store-color', [ColorController::class, 'storeColor'])->name('store.color');
Route::get('delete-color/{id}', [ColorController::class, 'deleteColor'])->name('delete.color');
Route::get('edit-color/{id}', [ColorController::class, 'editColor'])->name('edit.color');
Route::post('update-color/{id}', [ColorController::class, 'updateColor'])->name('update.color');

//Product
Route::get('product', [ProductController::class, 'index'])->name('product');
Route::get('add-product', [ProductController::class, 'addProduct'])->name('add.product');
Route::post('store-product', [ProductController::class, 'storeProduct'])->name('store.product');
Route::get('product-product/{id}', [ProductController::class, 'deleteProduct'])->name('delete.product');
Route::get('edit-product/{id}', [ProductController::class, 'editProduct'])->name('edit.product');
Route::post('update-product/{id}', [ProductController::class, 'updateProduct'])->name('update.product');
